//= require ../lib/_jquery
//= require ../lib/_clipboard.min
;(function () {
  'use strict';

  var token;

  function waitForForm(callback) {
    token = document.getElementById('token');
    if (token) {
      callback();
    }
    else {
      setTimeout(function() {
        waitForForm(callback);
      }, 100);
    }
  }

  waitForForm(function() {
    token.value = window.getApiKey();
    var clipboard = new ClipboardJS('#token-copy', {
      target: function() {
        return token;
      }
    });
    clipboard.on('success', function(e) {
      e.clearSelection();
      e.trigger.disabled = true;
      var text = e.trigger.textContent;
      e.trigger.textContent = 'Copied!';
      setTimeout(function() {
        e.trigger.disabled = false;
        e.trigger.textContent = text;
      }, 1000);
    });
  });
})();
