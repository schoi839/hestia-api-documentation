//= require ../lib/_jquery
;(function () {
  'use strict';

  var isProd = window.location.host === 'hestia.earth' || window.location.host === 'www.hestia.earth';
  var apiUrl = 'https://' + (isProd ? 'api.hestia.earth' : 'api-staging.hestia.earth');
  var webUrl = 'https://' + (isProd ? 'hestia.earth' : 'www-staging.hestia.earth');
  var docUrl = window.location.origin.replace('www', 'api' + '/docs');
  var apiKey = window.getApiKey();

  function findNodes(key) {
    return $('span:contains(' + key + '), code:contains(' + key + ')');
  }

  function updateAPIUrl() {
    var key = '<APIUrl>';

    findNodes(key).each(function(_i, elem) {
      elem.innerText = elem.innerText.replace(new RegExp(key, 'g'), apiUrl);
    });
    $('.nav-api-swagger-link').each(function(_i, elem) {
      elem.href = apiUrl + '/swagger';
    });
  }

  function updateWEBUrl() {
    var key = '<WEBUrl>';

    findNodes(key).each(function(_i, elem) {
      elem.innerText = elem.innerText.replace(new RegExp(key, 'g'), webUrl);
    });
  }

  function updateDOCUrl() {
    var key = 'DOCUrl';
    $('a[href="' + key + '"]').each(function(_i, elem) {
      elem.href = docUrl;
    });
  }

  function updateApiKey() {
    var key = '<your-token>';
    findNodes(key).each(function(_i, elem) {
      elem.innerText = elem.innerText.replace(new RegExp(key, 'g'), apiKey);
    });
    $('.logged-in').removeClass('is-hidden');
  }

  $(function() {
    updateAPIUrl();
    updateWEBUrl();
    updateDOCUrl();
    if (apiKey) {
      updateApiKey();
    }
  });
})();
