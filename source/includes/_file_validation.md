# Validate CSV/JSON files

Hestia provides some functions to validate a CSV / JSON / JSON-LD files formatted following the [Hestia format](/upload#excelCsv).

## Validating a CSV file

To validate a CSV file, you will first need to convert it to JSON.
This can be done using the Hestia's `utils` package:

1. Install `NodeJS` version `12`
2. Install the utils library globally: `npm install --global @hestia-earth/schema-convert`
3. Drop your CSV files into a specific `folder` then run:

<div class="full-column"></div>

```
hestia-convert-to-json folder
```

You will find in the `folder` the list of CSV files converted to JSON with the `.json` extension.
These files can be then used for validation described below.

## Validating the Terms

When uploading data on the Hestia platform, you will need to use our [Glossary of Terms](/glossary).
You can follow these steps to install a package to validate the terms:

1. Install `NodeJS` version `12`
2. Install the utils library globally: `npm install --global @hestia-earth/utils`
3. Drop your JSON / JSON-LD files into a specific `folder` then run:

<div class="full-column"></div>

```
API_URL=<APIUrl> hestia-validate-terms folder
```

Errors will appear in the console if any have been found.

## Validating the Schema

When uploading data on the Hestia platform, you will need to follow our [Schema](/schema).
You can follow these steps to install a package to validate the schema:

1. Install `NodeJS` version `12`
2. Install the schema validation library globally: `npm install --global @hestia-earth/schema-validation`
3. Drop your JSON / JSON-LD files into a specific `folder` then run:

<div class="full-column"></div>

```
hestia-validate-jsonld '' folder
```

Errors will appear in the console if any have been found.

## Validating the Data

One important step when uploading data on the Hestia platform is making sure the data is consistent using the [Data Validation package](https://gitlab.com/hestia-earth/hestia-data-validation).
You can follow these steps to install a package to validate the data:

1. Install `Python` version `3` minimum
2. Install the data validation library: `pip install hestia_earth.validation`
3. Drop your JSON / JSON-LD files into a specific `folder` then run:

<div class="full-column"></div>

```
API_URL=<APIUrl> VALIDATE_SPATIAL=false VALIDATE_EXISTING_NODES=true hestia-validate-data folder
```

Errors will appear in the console if any have been found.
