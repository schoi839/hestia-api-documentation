# Hestia Community Edition

We also provide a self-hosted version of the full Hestia calculations in our [Community Edition](https://gitlab.com/hestia-earth/hestia-community-edition).

## Getting Started

The easiest way to use the Community Edition is to run the Docker images on your computer or your own servers.
Please see the instructions [here](https://hub.docker.com/r/hestiae/ce-api).

If you have any questions or feedback, please create an issue [here](https://gitlab.com/hestia-earth/hestia-community-edition/-/issues/new).

You can also clone the [repository](https://gitlab.com/hestia-earth/hestia-community-edition) and install the Community Edition locally without Docker.
