# Hestia API

To access the Hestia API:

1. <a href="../users/signup" target="_blank">Create an Account</a>.
2. Retrieve your access token:

```shell
curl -X POST "<APIUrl>/users/signin" \
  -H "Content-Type: application/json" \
  --data '{"email":"email@email.com","password":"pwd"}' | jq '.token'
```

```python
import json
import requests

headers = {'Content-Type': 'application/json'}
body = json.dumps({"email":"email@email.com","password":"pwd"})
token = requests.post('<APIUrl>/users/signin', body, headers=headers).json().get('token')
print(token)
```

```typescript--browser
(async () => {
  const url = '<APIUrl>/users/signin';
  const headers = {
    'Content-Type': 'application/json'
  };
  const body = JSON.stringify({ email: 'email@email.com', password: 'pwd' });
  const response = await fetch(url, { method: 'POST', headers, body });
  const { token } = await response.json();
  console.log(token);
})();
```

```typescript--nodejs
const axios = require('axios');

(async () => {
  const url = '<APIUrl>/users/signin';
  const body = { email: 'email@email.com', password: 'pwd' };
  const { data: { token } } = await axios.post(url, body);
  console.log(token);
})();
```

You can now test the API <a class="nav-api-swagger-link" href='#'>on the Swagger docs</a>.

<aside class="logged-in is-hidden notice">
  To authenticate on the API, you can use your token:
  <div class="field has-addons">
    <div class="control is-expanded">
      <input id="token" class="input" value="<your-token>">
    </div>
    <div class="control">
      <button id="token-copy" class="button">Copy to clipboard</button>
    </div>
  </div>
</aside>

## Samples

For uploading data, you can view some samples [here](https://gitlab.com/hestia-earth/hestia-api/-/snippets).

> Retrieving your profile

```shell
curl -H "X-ACCESS-TOKEN: <your-token>" \
  -H "Content-Type: application/json" \
  "<APIUrl>/users/me"
```

```python
import requests

headers = {'Content-Type': 'application/json', 'X-ACCESS-TOKEN': '<your-token>'}
profile = requests.get('<APIUrl>/users/me', headers=headers).json()
print(profile)
```

```typescript--browser
(async () => {
  const url = '<APIUrl>/users/me';
  const headers = {
    'Content-Type': 'application/json',
    'X-ACCESS-TOKEN': '<your-token>'
  };
  const response = await fetch(url, { headers });
  const profile = await response.json();
  console.log(profile);
})();
```

```typescript--nodejs
const axios = require('axios');

(async () => {
  const url = '<APIUrl>/users/me';
  const headers = {
    'X-ACCESS-TOKEN': '<your-token>'
  };
  const { data } = await axios.get(url, { headers });
  console.log(data);
})();
```

> Downloading a node (example for a `Term`)

```shell
curl "<APIUrl>/terms/sandContent"
```

```python
import requests

headers = {'Content-Type': 'application/json'}
node = requests.get('<APIUrl>/terms/sandContent', headers=headers).json()
print(node)
```

```python
# or use our utils library
from hestia_earth.schema import SchemaType
from hestia_earth.utils.api import download_hestia

node = download_hestia('sandContent', SchemaType.TERM)
```

```typescript--browser
(async () => {
  const url = '<APIUrl>/terms/sandContent';
  const headers = {
    'Content-Type': 'application/json'
  };
  const response = await fetch(url, { headers });
  const node = await response.json();
  console.log(node);
})();
```

```typescript--nodejs
const axios = require('axios');

(async () => {
  const url = '<APIUrl>/terms/sandContent';
  const { data } = await axios.get(url);
  console.log(data);
})();
```

> Getting related nodes

```shell
curl "<APIUrl>/terms/sandContent/sites"
```

```python
import requests

headers = {'Content-Type': 'application/json'}
node = requests.get('<APIUrl>/terms/sandContent/sites', headers=headers).json()
print(node)
```

```python
# or use our utils library
from hestia_earth.schema import SchemaType
from hestia_earth.utils.api import find_related

cycles = find_related(SchemaType.TERM, 'sandContent', SchemaType.SITE)
```

```typescript--browser
(async () => {
  const url = '<APIUrl>/terms/sandContent/sites';
  const headers = {
    'Content-Type': 'application/json'
  };
  const response = await fetch(url, { headers });
  const node = await response.json();
  console.log(node);
})();
```

```typescript--nodejs
const axios = require('axios');

(async () => {
  const url = '<APIUrl>/terms/sandContent/sites';
  const { data } = await axios.get(url);
  console.log(data);
})();
```

All of our Nodes are linked together by a Graph Database, which means you can get a list of `Site` that are linked to a `Term` for example.

## Full Examples

<div class="full-column"></div>

```python
from hestia_earth.schema import SchemaType
from hestia_earth.utils.api import find_node, download_hestia

# this will give you partial information only
cycles = find_node(SchemaType.CYCLE, {
  'emissions.term.name': 'NO3, to groundwater, soil flux'
})

for cycle in cycles:
  # to retrieve the complete data of the cycle
  data = download_hestia(cycle['@id'], SchemaType.CYCLE, data_state='recaclulated')
  inputs = data.get('inputs', [])
  N_filter = [sum(input.get('value')) if input.get('term', {}).get('units') == 'kg N' else 0 for input in inputs]
  total = sum(N_filter)
  print(cycle, 'total nitrogen fertilizer', total)
```

<div class="full-column"></div>

```python
from hestia_earth.schema import SchemaType
from hestia_earth.utils.api import find_node, download_hestia
from hestia_earth.utils.model import find_term_match

# searching for aggregated GWP100 emissions on "Maize, grain"
impacts = find_node(SchemaType.IMPACTASSESSMENT, {
  'aggregated': 'true',
  'product.term.name': 'Maize, grain'
})
# only download the first impact to test, but there would be many more
data = download_hestia(impacts[0]['@id'], SchemaType.IMPACTASSESSMENT, data_state='aggregated')
# 'gwp100' here refers to the @id and not the name
emission = find_term_match(data['impacts'], 'gwp100')
print(emission['value'])
```

<div class="full-column"></div>

```typescript--browser
(async () => {
  const apiUrl = '<APIUrl>';
  const searchUrl = '<APIUrl>/search';
  const headers = {
    'Content-Type': 'application/json'
  };
  const body = JSON.stringify({
    fields: ['@id'],
    limit: 10,
    query: {
      bool: {
        'must': [
          { match: { '@type': 'Cycle' } },
          { match: { 'emissions.term.name': 'NO3, to groundwater, soil flux' } }
        ]
      }
    }
  });
  const { results: cycles } = await (await fetch(searchUrl, { method: 'POST', headers, body })).json();
  cycles.map(async cycle => {
    const data = await (await fetch(`${apiUrl}/cycles/${cycle['@id']}`, { headers })).json();
    const values = (data.inputs || [])
      .filter(input => input.term.units === 'kg N')
      .flatMap(input => input.value);
    const total = values.reduce((a, b) => a + b, 0);
    console.log(cycle, 'total nitrogen fertilizer', total);
  });
})();
```

<div class="full-column"></div>

```typescript--browser
(async () => {
  const apiUrl = '<APIUrl>';
  const searchUrl = '<APIUrl>/search';
  const headers = {
    'Content-Type': 'application/json'
  };
  // searching for aggregated GWP100 emissions on "Maize, grain"
  const body = JSON.stringify({
    fields: ['@id'],
    limit: 10,
    query: {
      bool: {
        'must': [
          { match: { '@type': 'ImpactAssessment' } },
          { match: { 'aggregated': true } },
          { match: { 'product.term': 'Maize, grain' } }
        ]
      }
    }
  });
  const { results: impacts } = await (await fetch(searchUrl, { method: 'POST', headers, body })).json();
  impacts.map(async impact => {
    const url = `${apiUrl}/impactassessments/${impact['@id']}?dataState=aggregated`;
    const data = await (await fetch(url, { headers })).json();
    const value = (data.impacts || []).find(impact => impact.term.name === 'GWP100').value;
    console.log(value);
  });
})();
```

<div class="full-column"></div>

```typescript--nodejs
const axios = require('axios');

(async () => {
  const apiUrl = '<APIUrl>';
  const searchUrl = '<APIUrl>/search';
  const body = {
    fields: ['@id'],
    limit: 10,
    query: {
      bool: {
        'must': [
          { match: { '@type': 'Cycle' } },
          { match: { 'emissions.term.name': 'NO3, to groundwater, soil flux' } }
        ]
      }
    }
  };
  const { data: { results: cycles } } = await axios.post(searchUrl, body);
  cycles.map(async cycle => {
    const { data } = await axios.get(`${apiUrl}/cycles/${cycle['@id']}`);
    const values = (data.inputs || [])
      .filter(input => input.term.units === 'kg N')
      .flatMap(input => input.value);
    const total = values.reduce((a, b) => a + b, 0);
    console.log(cycle, 'total nitrogen fertilizer', total);
  });
})();
```

<div class="full-column"></div>

```typescript--nodejs
const axios = require('axios');

(async () => {
  const apiUrl = '<APIUrl>';
  const searchUrl = '<APIUrl>/search';
  // searching for aggregated GWP100 emissions on "Maize, grain"
  const body = {
    fields: ['@id'],
    limit: 10,
    query: {
      bool: {
        'must': [
          { match: { '@type': 'ImpactAssessment' } },
          { match: { 'aggregated': true } },
          { match: { 'product.term': 'Maize, grain' } }
        ]
      }
    }
  };
  const { data: { results: impacts } } = await axios.post(searchUrl, body);
  impacts.map(async impact => {
    const url = `${apiUrl}/impactassessments/${impact['@id']}?dataState=aggregated`;
    const { data } = await axios.get(url);
    const value = (data.impacts || []).find(impact => impact.term.name === 'GWP100').value;
    console.log(value);
  });
})();
```
