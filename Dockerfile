FROM guillaumebriday/ruby-node:2.6.3

WORKDIR /app

RUN gem install bundler

COPY Gemfile .
COPY Gemfile.lock .

RUN bundle install

COPY . .

CMD bundle exec middleman server
