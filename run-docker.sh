#!/bin/sh

docker build --progress=plain \
  -t hestia-api-documentation:build \
  .

docker run --rm \
  --name hestia-api-documentation \
  -p 4567:4567 \
  -v ${PWD}/lib:/app/lib \
  -v ${PWD}/source:/app/source \
  hestia-api-documentation:build "$@"
