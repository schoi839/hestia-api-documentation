#!/usr/bin/env bash
shopt -s globstar

REPOSITORY="hestia-engine-models"
BRANCH="${1:-"develop"}"
DEST_FILE="source/includes/models_content.md"
GITLAB_URL="https://gitlab.com/hestia-earth/hestia-engine-models/-/blob"

# cloning repository if not exists
rm -rf $REPOSITORY
git clone --depth 1 https://gitlab.com/hestia-earth/${REPOSITORY}.git &>/dev/null
cd $REPOSITORY
git checkout $BRANCH

# concat all markdown within the models to source/includes folder
cd ../
SOURCE_FILES="$REPOSITORY/hestia_earth/models/**/*.md"
rm -rf $DEST_FILE
for f in $SOURCE_FILES
do
  (cat "${f}"; echo) >> $DEST_FILE;
  echo "[View source on Gitlab](${GITLAB_URL}/$BRANCH/${f:21})" >> $DEST_FILE;
  echo "" >> $DEST_FILE;
done
# replace headers to handle nesting
sed -i 's/# /## /' $DEST_FILE || sed -i '' 's/# /## /' $DEST_FILE

# set full column on the code
SED_SRC='\`\`\`python'
SED_DEST='\<div class=\"full-column\"\>\<\/div\>\n\n\`\`\`python'
sed -i "s|$SED_SRC|$SED_DEST|g" $DEST_FILE || sed -i '' "s/$SED_SRC/$SED_DEST/" $DEST_FILE

# indent sub-lists
SRC_INDENT="  "
DEST_INDENT="    "
sed -i "s|$SRC_INDENT$SRC_INDENT$SRC_INDENT$SRC_INDENT$SRC_INDENT$SRC_INDENT-|$SRC_INDENT$SRC_INDENT$DEST_INDENT$SRC_INDENT$SRC_INDENT$SRC_INDENT-|g" $DEST_FILE || sed -i '' "s|$SRC_INDENT$SRC_INDENT$SRC_INDENT$SRC_INDENT$SRC_INDENT$SRC_INDENT-|$SRC_INDENT$SRC_INDENT$DEST_INDENT$SRC_INDENT$SRC_INDENT$SRC_INDENT-|g" $DEST_FILE
sed -i "s|$SRC_INDENT$SRC_INDENT$SRC_INDENT$SRC_INDENT$SRC_INDENT-|$SRC_INDENT$SRC_INDENT$DEST_INDENT$SRC_INDENT$SRC_INDENT-|g" $DEST_FILE || sed -i '' "s|$SRC_INDENT$SRC_INDENT$SRC_INDENT$SRC_INDENT$SRC_INDENT-|$SRC_INDENT$SRC_INDENT$DEST_INDENT$SRC_INDENT$SRC_INDENT-|g" $DEST_FILE
sed -i "s|$SRC_INDENT$SRC_INDENT$SRC_INDENT$SRC_INDENT-|$SRC_INDENT$SRC_INDENT$DEST_INDENT$SRC_INDENT-|g" $DEST_FILE || sed -i '' "s|$SRC_INDENT$SRC_INDENT$SRC_INDENT$SRC_INDENT-|$SRC_INDENT$SRC_INDENT$DEST_INDENT$SRC_INDENT-|g" $DEST_FILE
sed -i "s|$SRC_INDENT$SRC_INDENT$SRC_INDENT-|$SRC_INDENT$SRC_INDENT$DEST_INDENT-|g" $DEST_FILE || sed -i '' "s|$SRC_INDENT$SRC_INDENT$SRC_INDENT-|$SRC_INDENT$SRC_INDENT$DEST_INDENT-|g" $DEST_FILE
sed -i "s|$SRC_INDENT$SRC_INDENT-|$SRC_INDENT$DEST_INDENT-|g" $DEST_FILE || sed -i '' "s|$SRC_INDENT$SRC_INDENT-|$SRC_INDENT$DEST_INDENT-|g" $DEST_FILE
sed -i "s|$SRC_INDENT-|$DEST_INDENT-|g" $DEST_FILE || sed -i '' "s|$SRC_INDENT-|$DEST_INDENT-|g" $DEST_FILE
